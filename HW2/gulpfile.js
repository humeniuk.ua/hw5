const {src, dest, watch, series} = require("gulp");
const htmlmin = require("gulp-htmlmin");
const gulp_sass = require('gulp-sass')(require('sass'));
const browsersync = require("browser-sync").create();
const gulp_uglify = require("gulp-uglify");
const gulp_clean_css = require("gulp-clean-css");
const gulp_concat = require("gulp-concat");
const gulp_autoprefixer = require("gulp-autoprefixer");
const gulp_clean_dir = require("gulp-clean-dir");
const gulp_css_minify =  require("gulp-css-minify");

function html (finish){
    src("src/index.html")
    .pipe(gulp_clean_dir("dist"))
    .pipe(htmlmin({ collapseWhitespace: true}))
    .pipe(dest("dist"));

 finish();
}

function scss(finish) {
    src("src/scss/*.scss")
    .pipe(gulp_sass({outputStyle: "compressed"}).on("error", ()=>{console.log("Error in sass code!");}))
    .pipe(gulp_autoprefixer({cascade: false}))
    .pipe(gulp_concat("styles.min.css"))
    .pipe(gulp_clean_css())
    .pipe(gulp_css_minify())
    .pipe(dest("dist"));

 finish();
}

function images(finish) {
  src("src/img/*.*").pipe(dest("dist" + "/img"))

  finish();
}

function js (finish){
     src("src/js/main.js")
    .pipe(gulp_uglify())
    .pipe(dest("dist"))

    finish();
}

function watcher() {
    browsersync.init({
      server: {
        baseDir: "dist"
      },
    });
    watch("src/**/*.*", series(html, scss, images, js)).on("change", browsersync.reload);
  }
  
  exports.scss = scss;
  exports.html = html;
 
  exports.default = series(html, scss, images, js, watcher);
 
  

